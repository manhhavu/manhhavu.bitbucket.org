---
layout: post
title:  "Illustrated composite pattern"
categories: design-pattern
---

When you want to combine some implementations of an interface into a single one, here comes [Composite pattern](http://en.wikipedia.org/wiki/Composite_pattern). There are already a lot of resources related to this pattern, so I won't bother you anymore with its theory side. I will only give some thoughts and examples about this powerful pattern.

# Sort by multiple conditions

Suppose that we have to sort a list of hotels by conditions as: user ratings, distance to position of interest, min price, ... Let's ignore some detail about domain modeling and assume that we have a hotel class like: 

{% highlight scala %}
case class Hotel (averageRating: Int,
    minPrice: BigDecimal,
    distanceInMeter: Int)
{% endhighlight %}

If we want to sort a list of hotels as descending order of user ratings, it's simple, just implement interface Comparator:

{% highlight scala %}
class DescendingUserRatingsComparator extends Comparator {
    def compare(h1: Hotel, h2: Hotel): Int = // calculate difference
} 

class AscendingMinPriceComparator extends Comparator {
    def compare(h1: Hotel, h2: Hotel): Int = // calculate difference
}

class AscendingDistanceComparator extends Comparator {
    def compare(h1: Hotel, h2: Hotel): Int = // calculate difference
}

// and now we can easily sort hotels

hotels.sortWith(new DescendingUserRatingComparator) // sort by user ratings
hotels.sortWith(new AscendingMinPriceComparator) // sort by min price
hotels.sortWith(new AscendingDistanceComparator) // sort by distance
{% endhighlight %}

Now users ask you an interesting requirement. They want that when they want sort by user ratings, the list is sort by user ratings (of course), but if two hotels have same ratings, they will be sorted by min price also. 

So your Comparator implementations now are not fit for the requirement, you might think of modifying DescendingUserRatingsComparator to add min price checking inside. If what you are thinking, you should be careful about this decision. What if these guys several months later will ask you to change min price checking to distance checking instead? Yoy will have to one more time DescendingUserRatingsComparator? 

